# Causa Theo Stratmann

## Hergen Hincke
<small>aus Oldenburg</small>

Instagram (privat): [https://www.instagram.com/hergen.hnke/](https://www.instagram.com/hergen.hnke/)<br>
Instagram (gewerblich): [https://www.instagram.com/hyped.hergen/](https://www.instagram.com/hyped.hergen/)<br>
Amazon Profil: https://www.amazon.de/gp/profile/amzn1.account.AFHTI7WD7BWU4DP3HOKLYX2E7IVQ

### Shops

- [Wavefront Agency](https://www.wavefront-agency.de/) (siehe [`wavefront-agency-impressum.png`](./pages/wavefront-agency-impressum.md))
- [Mr.Back (TM)](https://mr-back.de/) (siehe [`mr-back-impressum.png`](./pages/mr-back-impressum.md))
- [NiceSlides (TM)](https://nice-slides.de/) (siehe [`nice-slides-impressum.png`](./pages/nice-slides-impressum.md))
- [Mr.Cozy (TM)](https://mr-cozy.de/) (siehe [`mr-cozy-impressum.png`](./pages/mr-cozy-impressum.md))

Hergen stellt sich als 22-jähriger auf der Website seiner [Wavefront Agentur](https://www.wavefront-agency.de/) vor (siehe [`wavefront-agency-about.png`](./pages/wavefront-agency-about.md)).
Ein Blick auf die WaybackMachine [https://web.archive.org/](https://web.archive.org/) zeigt,
das in diesem August der erste Snapshot der ["About"-Seite](https://web.archive.org/web/20230501000000*/https://www.wavefront-agency.de/about/) gemacht wurde. 

Hergen Hincke hat in der STRG_F Dokumentation ebenfalls einen kleinen [Sprechbeitrag](https://youtu.be/MbJOQsK42iE?t=762) (https://youtu.be/MbJOQsK42iE?t=762), wird allerdings nur beim Vornamen genannt.
Die Verbindung zu Theo Stratmann kann per Verlinkung auf Instagram nachvollzogen werden.
Unter [https://www.instagram.com/p/CeyKQMPD6Ez/](https://www.instagram.com/p/CeyKQMPD6Ez/) ist Theo Stratmann zum Beispiel verlinkt worden (siehe [`instagram-verbindung.png`](./pages/instagram-verbindung.md)),
oder unter [https://www.instagram.com/p/Cf9VlUPI8iS/](https://www.instagram.com/p/Cf9VlUPI8iS/) ist Hergen Hincke (vermutlich die sich im Auto spiegelnde,
filmende Person) verlinkt (siehe [`instagram-verbindung-2.png`](./pages/instagram-verbindung-2.md)).

Datenabfrage zur Markenkennzeichnung

- [https://register.dpma.de/DPMAregister/marke/uebersicht](https://register.dpma.de/DPMAregister/marke/uebersicht)
- [http://tess2.uspto.gov/](http://tess2.uspto.gov/)

Die Verwendung von "Bekannt aus" aus jeder der Shop-Seiten von Hergen Hincke sind nicht nachvollziehbar (siehe [`mr-back-start.png`](./pages/mr-back-start.md), [`mr-cozy-start.png`](./pages/mr-cozy-start.md) und [`nice-slides-start.png`](./pages/nice-slides-start.md)).
Ich konnte nicht einen Nachweis darüber finden, dass die "Marken" Mr.Back, Mr.Cozy oder NiceSlides Erwähnung bei RTL, Galileo, Brigitte, Vogue, Glamour oder Cosmopolitan finden.
Sollte keine dieser "Marken" Erwähnung in den genannten Medien finden, dürften die Erwähnungen wettbewerbswidrig und irreführend sein.

Alle Firmen von Hergen Hincke führen im Impressum die gleiche Adresse als Firmensitz. Unter der gleichen Anschrift findet man ebenfalls das Unternehmen **Heinrich Hincke Landwirtschaft**.
Es ist naheliegend, dass es sich hier um Hergen Hinckes Vater oder Großvater handelt.

#### Mr.Back (TM)

Shopsystem: Shopify (https://mr-back.de/)<br>
Projekname: confihealth.myshopify.com

Die Marke wurde in Deutschland angemeldet. **Die TM Markenkennzeichnung ist legitim**.

[https://register.dpma.de/DPMAregister/marke/register/3020212350719/DE](https://register.dpma.de/DPMAregister/marke/register/3020212350719/DE)

Auf der Website von Mr.Back (siehe [`produkt-mr-back.png`](./pages/produkt-mr-back.md)) wird mit folgendem Slogan geworben:

> Die Geschichte begann im Jahr 2012. Wir setzten uns als Aufgabe, ein Produkt zu entwickeln um Rückenschmerzen und eine schlechte Haltung zu beseitigen. Damals teilten wir uns für die gemeinsame Arbeit noch einen einzelnen Schreibtisch. Heute sind wir ein Team aus vielen Mitarbeitern, die stetig neue Ideen und Verbesserungen in unser Unternehmen bringen.

Hergen Hincke, 22 Jahre jung, hat vor 11 Jahren damit begonnen ein Produkt zu entwickeln, das schlechte Haltung und Rückenschmerzen beseitigen soll, im Alter von 11 Jahren.

Das Produkt Mr.Back (siehe [`produkt-mr-back.png`](./pages/produkt-mr-back.md)) ist günstig bei [Alibaba](https://www.alibaba.com/product-detail/Back-Straightener-Posture-Corrector_1600585808020.html) (siehe [`alibaba-mr-back.jpg`](./pages/alibaba-mr-back.md)) verfügbar.

[https://www.alibaba.com/product-detail/Back-Straightener-Posture-Corrector_1600585808020.html](https://www.alibaba.com/product-detail/Back-Straightener-Posture-Corrector_1600585808020.html)

Ab einer Abnahme von mindestens 100 Stk. kostet ein Exemplar **$1.33** (~EUR 1,22). Inklusive Versand nach Deutschland (**$47.60**) liegt der Gesamtpreis bei **$180.60** (~EUR 166,10) für 100 Exemplare.

In der Anschaffung kostet ein Produkt somit **~EUR 1,66**.

Verkauft wird das Produkt pro Stk. zu je **EUR 27,90**.

Ein satte Marge von **~EUR 26,24**.

Die Trustpilot Bewertungen sprechen eine deutliche Sprache [https://de.trustpilot.com/review/mr-back.de?stars=1](https://de.trustpilot.com/review/mr-back.de?stars=1).

Die Produkte sind übrigens nie zum UVP erwerbbar, nach Ablauf einer Rabattaktion startet ein neuer Counter nach einer kurzen Meldung, dass die Rabattaktion gleich ausläuft.

Konkurrenz im gleichen Marktsegment gefällt Hergen Hincke ganz und gar nicht, was er sehr deutlich mit einer Rezension unter einem identischen Produkt zum Ausdruck bringt (siehe [`mr-back-bewertet-mitberwerber.png`](./pages/mr-back-bewertet-mitberwerber.md)). Wie Hergen Hincke so detailliert darlegen kann, dass weder Qualität noch Passform an das "Original" heranreichen, ist fraglich, da der Bewertung kein "Verfizierter Kauf" zugrunde liegt.

##### Whitelion Agency

Einem auf YouTube (siehe [`whitelion-mr-back-testimonial.mp4`](./pages/whitelion-mr-back-testimonial.md)) veröffentlichten Testimonial der Whitelion Agency vom 09.12.2021 zufolge, hat Hergen Hincke die Dienstleistungen der Whitelion Agency in Anspruch genommen. Maßgeblicher Grund für die Inanspruchnahme ist laut Website der Whitelion Agency folgender (siehe [`whitelion-agency.png`](./pages/whitelion-agency.md)).

> Zu wenig Zeit für das Hauptgeschäft und fehlende Expertise für die Skalierung auf hohe Monatsumsätze.

Aufmerksam ist Hergen Hincke auf die Whitelion Agency durch einen Freund geworden, der ebenfalls einen Onlishop betreibt und ebenfalls Kunde der Whitelion Agency ist (siehe [`whitelion-mr-back-testimonial.mp4`](./pages/whitelion-mr-back-testimonial.md) ab 0:56). Anhand weiterer Whitelions Agency Kundenstimmen (siehe [`whitelion-agency.png`](./pages/whitelion-agency.md)), bin ich über die Brand Küchenkompane auf dessen Inhaber Mats Jannik Radzuweit gestoßen. Hier besteht eine Verbindung zu Hergen Hincke (siehe [`mats-radzuweit-hergen-hincke-instagram.png`](./pages/mats-radzuweit-hergen-hincke-instagram.md) und [`mats-radzuweit-hergen-hincke-instagram-2.png`](./pages/mats-radzuweit-hergen-hincke-instagram-2.md)). Darüber hinaus hat Hergen Hincke über Trustpilot eine Bewertung über die Messer von Küchenkompane abgegeben ([`trustpilot-bewertungen-hergen-hincke.png`](./pages/trustpilot-bewertungen-hergen-hincke.md)).Ob es sich bei Mats Jannik Radzuweit um den Freund handelt, über den Hergen Hincke auf die Whitelion Agency aufmerksam wurde, kann ich nicht mit Sicherheit bestätigen. Ein genauer Blick in den Shop von Mats Jannik Radzuweit bring allerdings das gleiche Geschäftmodell zu Tage (siehe [Mats Jannik Radzuweit](#mats-jannik-radzuweit)).

#### Nice Slides (TM)

Shopsystem: Shopify (https://nice-slides.de/)<br>
Projekname: niceslide.myshopify.com

Die Marke wurde weder in Deutschland, noch in den USA angemeldet. **Die TM Markenkennzeichnung ist wettbewerbswidrig**.

Auf der Website von Nice Slides (siehe [`produkt-nice-slides.png`](./pages/produkt-nice-slides.md)) wird mit folgendem Slogan geworben:

> Die Geschichte begann im Jahr 2018. Wir setzten uns als Aufgabe, ein Produkt zu entwickeln, welches das beste aus Komfort und Langlebigkeit vereint. Damals teilten wir uns für die gemeinsame Arbeit noch einen einzelnen Schreibtisch. Heute sind wir ein Team aus vielen Mitarbeitern, die stetig neue Ideen und Verbesserungen in unser Unternehmen bringen.

Ob es sich wohl um den gleichen Schreibtisch handelt, wie der aus der Entwicklung von Mr.Back?

Das Produkt (siehe [`produkt-nice-slides.png`](./pages/produkt-nice-slides.md)) ist günstig bei [Alibaba](https://www.alibaba.com/product-detail/Thick-Platform-Bathroom-Home-Slippers-Women_1600698400688.html) (siehe [`alibaba-nice-slides.jpg`](./pages/alibaba-nice-slides.md)) verfügbar.
Es gibt sogar Übeschneidungen in den Produktbildern (siehe [`promo-image-alibaba.png`](./pages/promo-image-alibaba.md) und [`promo-image-nice-slides.png`](./pages/promo-image-nice-slides.md)).

[https://www.alibaba.com/product-detail/Thick-Platform-Bathroom-Home-Slippers-Women_1600698400688.html](https://www.alibaba.com/product-detail/Thick-Platform-Bathroom-Home-Slippers-Women_1600698400688.html)

Ein Exemplar kostet **$1.35** (~EUR 1,24).

Verkauft wird das Produkt pro Stk. zu je **EUR 28,95**.

Ein satte Marge von **~EUR 27,71**.

Zu erwähnen ist, dass die Versandkosten nicht auf der Verkaufsseite von Alibaba angezeigt werden und noch zusätzlich anfallen.

Es gibt einen Hinweis darauf, dass Nice Slides aus [CozySlides](https://www.cozyslides.de/) hervorgegangen ist.

Eine Bewertung auf Trustpilot deutet dies an (siehe [`trustpilot-cozy-slides.png`](./pages/trustpilot-cozy-slides.md)):

*Reklamation an Hergen Hincke persönlich*

*Hat Hergen Hincke auf Eure wiederholte Reklamation nicht geantwortet oder liefert er nicht? Vielleicht habt Ihr mehr Erfolg, wenn Ihr ihm direkt über seinen Instagram Account "Hergen | E-Commerce" anschreibt. Wenn er nicht gerade wieder Euer hart verdientes Geld in Dubai verprasst, hat er vielleicht auch mal Zeit Euch zu antworten*

Quelle: [https://de.trustpilot.com/review/cozyslides.de](https://de.trustpilot.com/review/cozyslides.de)

Darüberüber hinaus findet man in der WaybackMachine archive.org eine archivierte Seite, in der das typische TM im Produktnamen auftaucht (siehe [`cozyslides-trademark.png`](./pages/cozyslides-trademark.md)).

Quelle: [https://web.archive.org/web/20220527075315/https://www.cozyslides.de/password](https://web.archive.org/web/20220527075315/https://www.cozyslides.de/password)

#### Mr.Cozy (TM)

Shopsystem: Shopify (https://mr-cozy.de/)<br>
Projektname: mrcozy.myshopify.com

Die Marke wurde weder in Deutschland, noch in den USA angemeldet. **Die TM Markenkennzeichnung ist wettbewerbswidrig**.

[Mr Cozy](https://register.dpma.de/DPMAregister/marke/registerHABM?AKZ=018627453&CURSOR=0) existiert als Marke, Hergen Hincke ist jedoch nicht der Markeninhaber.

[https://register.dpma.de/DPMAregister/marke/registerHABM?AKZ=018627453&CURSOR=0](https://register.dpma.de/DPMAregister/marke/registerHABM?AKZ=018627453&CURSOR=0)

Von einer detaillierte Darstellung zu den Produkten von Mr.Cozy nehme ich an dieser Stelle Abstand. Die verwendete Masche ist die gleiche, wie bei den Shops Mr.Back und Nice Slides.

[https://mr-cozy.de/products/mr-cozy%E2%84%A2-winter-leggings](https://mr-cozy.de/products/mr-cozy%E2%84%A2-winter-leggings)<br>
[https://de.aliexpress.com/item/1005003639811480.html](https://de.aliexpress.com/item/1005003639811480.html)

[https://mr-cozy.de/products/mr-cozy](https://mr-cozy.de/products/mr-cozy)<br>
[https://de.aliexpress.com/i/4000036379845.html](https://de.aliexpress.com/i/4000036379845.html)

## Mats Jannik Radzuweit

Instagram: https://www.instagram.com/matsradzuweit/ (siehe [`mats-radzuweit-instagram.png`](./pages/mats-radzuweit-instagram.md))

Verbindung zu Hergen Hincke 

- [`mats-radzuweit-hergen-hincke-instagram.png`](./pages/mats-radzuweit-hergen-hincke-instagram.md)
- [`mats-radzuweit-hergen-hincke-instagram-2.png`](./pages/mats-radzuweit-hergen-hincke-instagram-2.md)

### Firmen

- Radzuweit Industries GmbH
- Debeness UG

### Shops

#### Küchenkompane

Shopsystem: Shopify<br>
Projekname: fiew-kitchen.myshopify.com

Von einer detaillierte Darstellung zu den Produkten von Küchenkompane nehme ich an dieser Stelle Abstand. Die verwendete Masche ist die gleiche, wie bei den Shops Mr.Back, Nice Slides und Mr.Cozy. Auch hier gibt es wieder Überschneidungen in den Produktbildern.

https://kuechenkompane.de/products/premium (siehe [`kuechenkompane-produkt.png`](./pages/kuechenkompane-produkt.md))<br>
https://de.aliexpress.com/item/1005005110532499.html (siehe [`kuechenkompane-ali-express.png`](./pages/kuechenkompane-ali-express.md))

> Es begann 2009 in einer kleinen Werkstatt. Damals gab es nur uns und einen Schreibtisch. Heute begeistern wir ganz Deutschland mit unseren Produkten.

2009 war Mats Jannik Radzuweit gerade einmal 9 Jahre alt.

Bewertungen über die Produktqualität fallen nicht gut aus und sprechen für sich (siehe [`kuechenkompane-reviews-facebook.png`](./pages/kuechenkompane-reviews-facebook.md)).

##### Whitelion Agency

Mats Jannik Radzuweit hat ebenfalls die Dienstleitungen der Whitelion Agency in Anspruch genommen (siehe [`whitelion-agency.png`](./pages/whitelion-agency.md)). Grund hierfür war folgender.

> Kein professioneller Partner, welcher die Expertise besitzt, um das Facebook Marketing im hohen Werbe-Budget profitabel zu skalieren.

Ein entsprechendes Testimonial Video wurde auf YouTube am 16.01.2021 veröffentlicht (siehe [`whitelion-kuechenkompane-testimonial.mp4`](./pages/whitelion-kuechenkompane-testimonial.md)).

#### Debeness

Shopsystem: Shopify (https://debeness.de/)<br>
Projekname: debeness-de.myshopify.com

Instagram: https://www.instagram.com/debeness/

Dieser Shopify Shop ist nicht mehr erreichbar. Inhalte des Shops können in der Yandex Suchmaschine gefunden werden.

Yandex Cache<br><small>
Beispiel: https://yandex.com/search/?text=https%3A%2F%2Fdebeness.de%2Fpages%2Fuber-uns</small>

- [`debeness-ueber-uns.png`](./pages/debeness-ueber-uns.md)
- [`debeness-impressum.png`](./pages/debeness-impressum.md)

## Jonas Peine

<small>aus Oldenburg</small>

Instagram: [https://www.instagram.com/jonas.handsome/](https://www.instagram.com/jonas.handsome/)<br>
Instagram: [https://www.instagram.com/jonas.peine.ecom/](https://www.instagram.com/jonas.peine.ecom/)<br>
Instagram: [https://www.instagram.com/_hey.handsome](https://www.instagram.com/_hey.handsome)

Jonas Peine scheint älter zu sein als Theo Stratmann und Hergen Hincke. Einer seiner "erfolgreicheren" Posts bei Facebook stammt aus dem Jahr 2016 und lässt ihn in keinem so guten Licht dastehen (siehe [`jonas-peine-facebook-2016.png`](./pages/jonas-peine-facebook-2016.md#jonas-peine-facebook-2016),
[`jonas-peine-facebook-2016-comment-1.png`](./pages/jonas-peine-facebook-2016.md#jonas-peine-facebook-2016-comment-1) und [`jonas-peine-facebook-2016-comment-2.png`](./pages/jonas-peine-facebook-2016.md#jonas-peine-facebook-2016-comment-2)).
Der Post ist leider nicht mehr öffentlich einsehbar, oder wurde komplett gelöscht.

Jonas Peine hat in der Vergangenheit mindestens eine Afterwork Veranstaltung im Hotel Rosenbohm ausgerichtet (siehe [`jonas-peine-afterwork-hotelrosenbohm.png`](./pages/jonas-peine-afterwork-hotelrosenbohm.md#jonas-peine-afterwork-hotelrosenbohm) und [`jonas-peine-afterwork-hotelrosenbohm-aka-touched-hospitality.png`](./pages/jonas-peine-afterwork-hotelrosenbohm.md#jonas-peine-afterwork-hotelrosenbohm-aka-touched-hospitality)).

Unter der Prämisse, dass Theo Stratmann der Sohn von Kristin Rosenbohm-Stratmann und Lutz Stratmann ist, schließt sich spätestens hier der Kreis.

## Theo Stratmann

<small>aus Oldenburg</small>

Linktree: [https://linktr.ee/theo_stratmann](https://linktr.ee/theo_stratmann)<br>
Instagram: [https://www.instagram.com/theo_stratmann/](https://www.instagram.com/theo_stratmann/)<br>
Instagram: [https://www.instagram.com/prinztheogregorvonstratmann/](https://www.instagram.com/prinztheogregorvonstratmann/)

Laut [Bild](https://www.bild.de/regional/hamburg/hamburg-aktuell/deutschlands-groesster-schnoesel-teenie-theo-18-giesst-blumen-mit-champagner-85046476.bild.html) Informationen verdient Theo Stratmann, nach eigenen Angaben
als stiller Teilhaber, mit einem Online-Shop für Rückenprodukte sein Geld (siehe [`bild-artikel.png`](./pages/bild-artikel.md)).
Hier kommt nun Hergen Hincke mit seinem Online-Shop [Mr.Back](https://mr-back.de/) (https://mr-back.de/) ins Spiel.
Nachweisen lässt sich für mich nicht, dass Theo Stratmann stiller Teilhaber bei [Mr.Back](https://mr-back.de/) ist,
die Indizien sprechen allerdings dafür. Ob und inwieweit Theo Stratmann im Hintergrund ebenfalls still an Nice Slides und Mr.Cozy beteiligt ist, bleibt offen.
Abgesehen von den widersprüchlichen Statements in den sozialen Medien, ist das Geschäftsmodell klar erkennbar. Billig in China produzieren und überteuert in Deutschland verkaufen.
Die Grundlage der überzogenen Inszenierung seiner selbst, um auf die aktuelle Situation von Natur und Klimawandel aufmerksam zu machen, wird durch das Geschäftsmodell ad absurdum geführt und ist damit klar als Ausrede entlarvt.
Ebenfalls verwerflich ist der bewusste oder unbewusste Umgang mit den Arbeitsbedingungen im asiatischen Raum zu bewerten.

Sollte Theo Stratmann in der Entwicklungphase von Mr.Back vor 11 Jahren dabeigewesen sein, war der heute 18-jährige zu der Zeit gerade einmal 7 Jahre jung.

Seine aktuelle Medienpräsenz hat Theo Stratmann übrigens zum Anlass genommen, sein überaus erfolgreiches Business-Modell in Form von Coachings weiter zu monetarisieren.
Das Impressum von Funnnel Cockpit verweist auf ein virtuelles Büro in einem Hamburg Business Center (siehe [`funnelcockpit-impressum.png`](./pages/funnelcockpit-impressum.md)).

Funnel Cockpit: [https://theo-stratmann.funnelcockpit.com/](https://theo-stratmann.funnelcockpit.com/) (siehe [`instagram.png`](./pages/instagram.md) und [`funnelcockpit.png`](./pages/funnelcockpit.md))

Der YouTuber Torben Platzer (https://www.youtube.com/@TorbenPlatzer) hat sich das Business Coaching von Theo Stratmann gekauft.
In dem Video [Ich habe das Business Coaching von Theo Stratmann (STRG F) gekauft!](https://youtu.be/em4firWNvuw), veröffentlicht am 27.08.2023, wird bestätigt, dass das Business-System darauf aufbaut, Ware güntig in China einzukaufen und teuer in Deutschland zu verkaufen.
Theo Stratmann spricht in diesem Kurs vermeindlich genau 30 Sekunden, anschließend übernimmt Hergen Hincke. Hier wird ganz offensichtlich die mediale Aufmerksamkeit genutzt und Theo Stratmann ist das Gesicht der "Kampagne".

Persönlich habe ich das Gefühl, das dieses Modell des Geldmachens in den vergangenen Jahre stark zugenommen hat. Von Zahnaufhellungsprodukten über Schallzahnbürsten bis hin zu Massagepistolen, ist das Produktportfolio diser Kundenverarschung schier unendlich.

Eigentlich Theo Rosenbohm-Stratmann (siehe [`theo-rosenbohm-google.png`](./pages/theo-rosenbohm-google.md) und [`theo-rosenbohm.png`](./pages/theo-rosenbohm.md))?

Theo wird ebenfalls mit dem nicht mehr verfügbaren Onlineshop noor-home.com (Shopify, interner Projektname `young-rich-london.myshopify.com`) in Verbindung gebracht,
auf Grund einer Facebookseite (siehe [`noor-home-facebook.png`](./pages/noor-home-facebook.md)). Über diesen, nicht mehr existenten Shop,
kann eine Verbindung zu **Jonas Peine** hergestellt werden (siehe [`noor-home-jonas-peine.jpg`](./pages/noor-home-jonas-peine.md) und [`noor-home-jonas-peine.mp4`](./pages/noor-home-jonas-peine.md)).
Eine weitere Verbindung kann über den ebenfalls nicht mehr existenten Shop theclassywall.com (ebenfalls Shopify) zu **Jonas Peine** hergestellt werden.

Bing Suche: `site:noor-home.com`

-> Allgemeine Geschäftsbedingungen (Seite im Suchcache gespeichert)

-> "The Classy Wall" innerhalb des Textes (siehe [`noor-home-allgemeine-geschaeftsbedingungen.png`](./pages/noor-home-allgemeine-geschaeftsbedingungen.md))

The Classy Wall verfügt noch über einen Instagram Account, aus dem hervorgeht, dass **Jonas Peine** zumindest an der Erstellung des Shops theclassywall.com betiligt war (siehe [`the-classy-wall-jonas-peine.mp4`](./pages/the-classy-wall-jonas-peine.md)).

Instagram Highlights von Jonas Peine mit Hergen Hincke und / oder Theo Stratmann:

- [`jonas-peine-connection-1.mp4`](./pages/jonas-peine-connection-1.md)
- [`jonas-peine-connection-2.mp4`](./pages/jonas-peine-connection-2.md)
- [`jonas-peine-connection-3.mp4`](./pages/jonas-peine-connection-3.md)
- [`jonas-peine-connection-4.mp4`](./pages/jonas-peine-connection-4.md)
- [`jonas-peine-connection-5.mp4`](./pages/jonas-peine-connection-5.md)
- [`jonas-peine-connection-6.mp4`](./pages/jonas-peine-connection-6.md)
- [`jonas-peine-connection-7.mp4`](./pages/jonas-peine-connection-7.md)
- [`instagram-story-jonas-peine-hergen-hincke-theo-stratmann.jpg`](./pages/instagram-story-jonas-peine-hergen-hincke-theo-stratmann.md)

Der vermutlich weniger erfolgreiche Shop [https://kochchampion.de](https://kochchampion.de) beinhaltet ebenfalls eine schlecht kopierte Variante der Allgemeinen Geschäftsbedingungen des Shops theclassywall.com (siehe [`kochchampion-allgemeine-geschaeftsbedingungen.png`](./pages/kochchampion-allgemeine-geschaeftsbedingungen.md)).
Ob und inwieweit zu dem dort im Impressum aufgeführten M.Hoyer aus Hamburg besteht, habe ich nicht weiter verfolgt.

[https://www.merchantgenius.io/shop/url/noor-home.com](https://www.merchantgenius.io/shop/url/noor-home.com)<br>
[https://www.merchantgenius.io/shop/url/theclassywall.com](https://www.merchantgenius.io/shop/url/theclassywall.com)

### Prämisse: Theo Stratmann ist der Sohn von Kristin Rosenbohm-Stratmann und Lutz Stratmann

Berücksichtigt man die Riege, aus der Theo Stratmann entsprungen ist, wird man nicht leugnen können, dass Zweit- und Drittnamen in solchen Kreisen gängig sind.
Selbstverständlich bediene ich hier ein Vorurteil, ein Klischee. Zieht man allerdings das Instagram Profil [`theo-rosenbohm-google.png`](./pages/theo-rosenbohm-google.md) heran,
ist diese Annahme nicht abwegig und Theo hat den weiteren Namen **Gregor**. Also warum dem Kind nicht auch **Justus** als weiteren Namen geben (siehe [`justus-stratmann.png`](./pages/justus-stratmann.md))?
Das wäre schon schicksalhafte Ironie (der nächste [BWL-Justus](https://bwl-justus.de/)). Dieser Quelle nach wäre Justus Geburtstag am 14.01.2005, und er wäre heute 18 Jahre alt, so wie unser Theo.

### Theo Gregor Justus (Rosenbohm-)Stratmann

Nach weiteren Recherchen lege ich mich fest. Bei Theo Stratmann handelt es sich um Theo Gregor Justus (Rosenbohm-)Stratmann (siehe [`theo-gregor-justus-stratmann.png`](./pages/theo-gregor-justus-stratmann.md)).

#### Eltern

##### Kristin Sigrid Rosenbohm(-Stratmann)

Theos Mutter betreibt ein Designerhotel in Oldenburg, betrieb früher zudem das Möbelhaus Rosenbohm (allem Anschein nach existiert die Firma weiterhin).
Darüber hinaus vermietet Frau Stratmann Ferienwohnungen auf der Ostseeinsel Spiekeroog (siehe [`meerlust-spiekeroog-impressum.png`](./pages/meerlust-spiekeroog-impressum.md)) über die Website [meerlust-spiekeroog.de](http://www.meerlust-spiekeroog.de/).
Anfang des Jahres, am 09.01.2023, hat Frau Rosenbohm zudem die TR Loop UG gegründet. Gegenstand des Unternehmens ist:

> Erstellung und Vermarktung von Internetseiten (inkl. Social Media), Modedesign, der Handel mit Bekleidung, der Handel mit und der Betrieb von E-Zigarettenautomaten sowie die Programmierung und der Betrieb von Online-Shops.

Erkennt man hier bereits die weiteren, zukünftigen Geschäftsgebiete, die Sohn Theo zusammen mit der Gruppe, bestehend aus Hergen Hincke und Jonas Peine, betreten wird?

Die TR Loop UG, sowie die Lutz Stratmann Beratungen UG weisen den identischen Adressdatensatz auf.
Laut Theo besteht jedoch seit langer Zeit kein Kontakt mehr zwischen Theo und seinem Vater.

##### Lutz Stratmann

Vater Lutz Stratmann ist selbständiger Rechtsanwalt, Unternehmensberater und CDU-Politiker.

- https://de.wikipedia.org/wiki/Lutz_Stratmann
- https://taz.de/Teure-Freundschaftsdienste/!5092238/

<!-- #### Großeltern -->

<!-- ##### Gislinde Rosenbohm -->

<!-- ##### Dieter Rosenbohm -->


**To be continued ...**